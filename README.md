完整记账系统的分离CMS，采用sapper和svelte开发，使用sb-admin2开源bootstrap4版本模板。

简单的记账系统，也是客户管理系统，也有商品库存中心，商品分类，系统用户管理等。

原则上二开情况下，可作为任何类型的CMS使用，后端对应二开即可。

对应后端（配合使用）：https://gitee.com/quazero/bee-cash

修改src/routes/api2/_config.js
```
const config = {
  host: 'http://127.0.0.1:3000',  //设置成后端api绑定的链接，比如https://api.efvcms.com
  redisClient: client,
};
```

修改/src/routes/_config.js
```
const config = {
  host: 'http://127.0.0.1:3000',  //同上设置成API后端的域名。
  imgHost: 'http://127.0.0.1:3000',  //预留，以后可能用来直接引用CDN地址。
};
```

git clone之后，安装好redis和pm2之后，直接跑yarn start即可！

pm2已经设置好，若需修改端口，请修改ecosystem.config.js中的env_production下面。

## 截图
![记账及客户管理系统](./screenshots/1.png)
![记账及客户管理系统](./screenshots/2.png)
![记账及客户管理系统](./screenshots/3.png)
![记账及客户管理系统](./screenshots/4.png)
![记账及客户管理系统](./screenshots/5.png)
![记账及客户管理系统](./screenshots/6.png)
![记账及客户管理系统](./screenshots/7.png)
![记账及客户管理系统](./screenshots/8.png)
![记账及客户管理系统](./screenshots/9.png)
![记账及客户管理系统](./screenshots/10.png)
![记账及客户管理系统](./screenshots/11.png)
![记账及客户管理系统](./screenshots/12.png)
![记账及客户管理系统](./screenshots/13.png)
![记账及客户管理系统](./screenshots/14.png)
![记账及客户管理系统](./screenshots/15.png)
![记账及客户管理系统](./screenshots/16.png)
![记账及客户管理系统](./screenshots/17.png)
![记账及客户管理系统](./screenshots/18.png)