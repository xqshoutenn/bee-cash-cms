import config from './_config.js';
import axios from 'axios';
const host = config.host;
const client = config.redisClient;
const url = host + '/api2/products';
const perPage = 24;
const initPage = 1;
const pageOneUrl = `${url}?page=${initPage}&limit=${perPage}`;

export async function get(req, res) {
  let { id } = req.query;
  let { page, limit, q } = req.query;
  if (!page) {
    page = initPage;
  }
  if (!limit) {
    limit = perPage;
  }
  const token = req.session.token;
  if(!token) {
    return false;
  }
  let requestUrl = url;
  if(id) {
    requestUrl = url + '/' + id;
  } else {
    requestUrl = `${url}?page=${page}&limit=${limit}`;
    if(q) {
      requestUrl += '&q=' + q;
    }
  }
  res.writeHead(200, {
    'content-type': 'text/plain;charset=utf8'
  });
  client.get(requestUrl, async function (err, response) {
    if (err) {
      console.log(err);
    }
    if (response) {
      return res.end(response);
    }
    try {
      const result = await axios({
        method: 'get',
        url: encodeURI(requestUrl),
        headers: {
          'Authorization': 'Bearer ' + token,
        },
        dataType: 'json'
      });
      let data = result.data;
      client.setex(requestUrl, 600, JSON.stringify(data));
      res.end(JSON.stringify(data));
    } catch (error) {
      res.end(JSON.stringify({ error: error.message }));
    }
  })
}
export async function put(req, res) {
  const token = req.session.token;
  if(!token) {
    return false;
  }
  res.writeHead(200, {
    'content-type': 'text/plain;charset=utf8'
  });
  const {title, counts, category, id, brand, pid} = req.body
  try {
    const result = await axios({
      method: 'put',
      url: url,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      data: {
        title,
        counts,
        category,
        brand,
        pid,
        id: id,
      },
      dataType: 'json'
    });
    let data = result.data;
    if(data.success==0) {
      return res.end(JSON.stringify(data));
    } else {
      client.del(pageOneUrl);
      return res.end(JSON.stringify(data));
    }
  } catch (error) {
    console.log(error);
    res.end(JSON.stringify({ error: error.message }));
  }
}
export async function post(req, res) {
  const token = req.session.token;
  if(!token) {
    return false;
  }
  res.writeHead(200, {
    'content-type': 'text/plain;charset=utf8'
  });
  const {title, counts, category, brand, pid} = req.body
  try {
    const result = await axios({
      method: 'post',
      url: url,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      data: {
        title,
        counts,
        brand,
        pid,
        category
      },
      dataType: 'json'
    });
    let data = result.data;
    if(data.success==0) {
      return res.end(JSON.stringify(data));
    } else {
      client.del(pageOneUrl);
      return res.end(JSON.stringify(data));
    }
  } catch (error) {
    console.log(error);
    res.end(JSON.stringify({ error: error.message }));
  }
}
export async function del(req, res) {
  const token = req.session.token;
  if(!token) {
    return false;
  }
  res.writeHead(200, {
    'content-type': 'text/plain;charset=utf8'
  });
  const id = req.query.id;
  try {
    const result = await axios({
      method: 'delete',
      url: url + '?id=' + id,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      dataType: 'json'
    });
    let data = result.data;
    if(data.success==0) {
      return res.end(JSON.stringify(data));
    } else {
      client.del(pageOneUrl);
      return res.end(JSON.stringify(data));
    }
  } catch (error) {
    console.log(error);
    res.end(JSON.stringify({ error: error.message }));
  }
}