import config from './_config.js';
import axios from 'axios';

export async function post(req, res) {
  const host = config.host;
  const token = req.session.token;
  if(!token) {
    return false;
  }
  const {id, password} = req.body;
  res.writeHead(200, {
    'content-type': 'text/plain;charset=utf8'
  });
  try {
    const result = await axios({
      method: 'post',
      url: host + `/api2/changepwd`,
      headers: {
        'Authorization': 'Bearer ' + token,
      },
      data:{id, password},
      dataType: 'json'
    });
    let data = result.data;
    if(data.success==0) {
      return res.end(JSON.stringify(data));
    } else {
      req.session.token = data.token;
      req.session.user = data.user;
      return res.end(JSON.stringify(data));
    }
  } catch (error) {
    console.log(error);
    res.end(JSON.stringify({ error: error.message }));
  }
}