import config from './_config.js';
import axios from 'axios';

export async function post(req, res) {
  res.writeHead(200, {
    'content-type': 'text/plain;charset=utf8'
  });
  try {
    req.session.token = null;
    req.session.user = null;
    return res.end(JSON.stringify({
      success: 1
    }));
  } catch (error) {
    res.end(JSON.stringify({ error: error.message }));
  }
}